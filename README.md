# Disord Grounding Bot

Uses Discord.js.
This bot uses interactions only so doesn't need any special intents and doesn't "listen" to chat.


## Required Bot Permissions

Invite scopes:
 * applications.commands
 * bot

Bot Permissions:
 * Manage Webhooks
