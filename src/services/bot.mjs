import {Client, Guild} from 'discord.js';
import GroundingManager from '../classes/GroundingManager.mjs';

export class Bot {
  /** @var {Client<true>} */
  client;

  /** @var {Guild} */
  guild;

  /** @var {string} */
  botRoot;

  /** @var {GroundingManager} */
  groundingManager;
}

/** Container for accessing global services. */
export const bot = new Bot();
