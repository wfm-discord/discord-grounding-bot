// src/events/interactionCreate.mjs

import {handleCommandInteraction} from 'bot-commons-utils/src/utils/interactionCreateUtil.mjs';

export default {
  name: 'interactionCreate',
  async execute(interaction, bot) {
    // console.log(`${interaction.user.tag} in #${interaction.channel.name} triggered an interaction.`);
    if (interaction.isChatInputCommand()) {
      await handleCommandInteraction(interaction, bot);
    } else if (interaction.isButton()) {
      await buttonInteraction(interaction, bot);
    }
  },
};

async function buttonInteraction(interaction, bot) {
  let original_command = interaction.message.interaction.commandName;

  const command = interaction.client.commands.get(original_command);
  if (!command) return;

  try {
    await bot.groundingManager.buttonPress(interaction, bot);
  } catch (error) {
    console.error(error);
    await interaction.reply({ content: 'There was an error while executing this button press!', ephemeral: true });
  }
}
