// src/commands/ground_me.mjs
import { SlashCommandBuilder } from 'discord.js';

const fast = 1500;
const normal = 1800;
const slow = 2500;
const very_slow = 5000;

export const data = new SlashCommandBuilder()
  .setName('ground_me')
  .setDescription('Starts grounding')
  .addIntegerOption(option =>
    option.setName('duration')
      .setDescription("How many minutes you're grounded for. MAX 12!")
      .setRequired(true)
  )
  .addStringOption(option =>
    option.setName('speed')
      .setDescription('How fast are you at clicking?')
      .setRequired(false)
      .addChoices(
        { name: 'fast', value: `${fast}` },
        { name: 'normal', value: `${normal}` },
        { name: 'slow', value: `${slow}` },
        { name: 'very slow', value: `${very_slow}` },
      )
  )
  .addUserOption(option =>
    option.setName('user')
      .setDescription('Who are you doing this grounding for?')
      .setRequired(false)
  );

export async function execute(interaction, bot) {
  await bot.groundingManager.groundUser(
    interaction,
    interaction.options.getInteger('duration'),
    interaction.options.getString('speed'),
    interaction.options.getUser('user')
  );
}

export async function pressTheRightButtonInteraction(interaction, bot) {
  await bot.groundingManager.pressTheRightButtonInteraction(interaction);
}
