import { SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('active')
  .setDescription('Are there active groundings going on?');

export async function execute(interaction, bot) {
  const user = interaction.user;
  const groundings = bot.groundingManager.getGrounding();
  if (!groundings) {
    await interaction.reply({content: 'There are no active groundings.'});
    return;
  }
  let activeCount = 0;
  let userActive = false;
  let userGrounding = null;
  for (let key in groundings) {
    const grounding = groundings[key];
    // check if grounding is still active
    if (grounding.isExpired()) {
      // I don't know if I want to have this delete the object yet
      // So just continue, and leaving the delete commented
      //delete groundings[key];
      continue;
    }
    activeCount++;
    // check if grounding belongs to user
    if (user.id === key) {
      userActive = true;
      userGrounding = grounding;
    }
  }
  if (activeCount > 0) {
    let reply = `There ${activeCount > 1 ? 'are' : 'is'} ${activeCount} active grounding${activeCount > 1 ? 's' : ''}.`;
    if (userActive) {
      if (userGrounding.isSurpriseSession()) {
        reply += ' Enjoy the rest of your surprise minutes left!';
      } else {
        const timeLeft = userGrounding.getMinutesLeft();
        reply += ` Your grounding has ${timeLeft} minute${timeLeft > 1 ? 's' : ''} left.`;
      }
    }
    await interaction.reply({content: reply});
    return;
  }
  await interaction.reply({content: 'There are no active groundings.'});
}
