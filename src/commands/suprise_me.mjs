// src/commands/surprise_me.mjs
import { SlashCommandBuilder } from 'discord.js';
import _ from 'lodash';
import {bot} from '../services/bot.mjs';

const fast = 1500;
const normal = 1800;
const slow = 2500;
const very_slow = 5000;

export const data = new SlashCommandBuilder()
  .setName('surprise_me')
  .setDescription('Starts a grounding where you don\'t know the duration (max 12 minutes).')
  .addStringOption(option =>
    option.setName('speed')
      .setDescription('How fast are you at clicking?')
      .setRequired(false)
      .addChoices(
        { name: 'fast', value: `${fast}` },
        { name: 'normal', value: `${normal}` },
        { name: 'slow', value: `${slow}` },
        { name: 'very slow', value: `${very_slow}` },
      )
  )
  .addUserOption(option =>
    option.setName('user')
      .setDescription('Who are you doing this grounding for?')
      .setRequired(false)
  );

export async function execute(interaction, bot) {
  await bot.groundingManager.groundUser(
    interaction,
    _.random(1, 12), // 1-12 minutes
    interaction.options.getString('speed'),
    interaction.options.getUser('user'),
    true
  );
}

export async function pressTheRightButtonInteraction(interaction, bot) {
  await bot.groundingManager.pressTheRightButtonInteraction(interaction);
}
