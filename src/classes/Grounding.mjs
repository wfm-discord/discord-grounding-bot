// src/classes/Grounding.mjs
import _ from 'lodash';

export default class Grounding {
  constructor(user, duration, isSurprise = false) {
    this.minutes = duration;
    this.grounding_duration = this.minutes * 60 * 1000;
    this.min_wait = 3000; // 3 seconds
    this.isSurprise = isSurprise;

    this.lastButtonPressTime = 0;
    this.buttonCooldown = 500; // Cooldown period in milliseconds

    this.startTime = Date.now();
    this.endTime = this.startTime + this.grounding_duration;

    // Max wait is the grounding duration divided by
    // a random number between low and high denominators
    let low_denominator = 3;
    let high_denominator = 6;
    // Change low and high change based on duration, as well.
    if (this.minutes <= 2) {
      low_denominator = 2;
      high_denominator = 4;
    } else if (this.minutes >= 6) {
      high_denominator = 9;
    }

    // change what the max wait can be based on grounding duration and randomness
    let r = _.random(low_denominator, high_denominator, 1);
    this.max_wait = this.grounding_duration / r;

    this.right = 0;
    this.wrong = 0;
    this.missed = 0;
    this.total = 0;
    this.extra = '';
  }

  isSurpriseSession() {
    return this.isSurprise;
  }

  canPressButton() {
    const now = Date.now();
    if (now - this.lastButtonPressTime < this.buttonCooldown) {
      return false; // Too soon since the last button press
    }
    this.lastButtonPressTime = now;
    return true;
  }

  getMinutes() {
    return this.minutes;
  }

  getMinutesLeft() {
    const now = Date.now();
    const timeLeft = this.endTime - now;
    const minutesLeft = Math.ceil(timeLeft / (1000 * 60));
    return minutesLeft;
  }

  getGroundingDuration() {
    return this.grounding_duration;
  }

  isExpired() {
    return Date.now() > this.endTime;
  }

  getRandomWait(time_so_far) {
    let wait = Math.floor(_.random(this.min_wait, this.max_wait));
    let time_left = this.grounding_duration - time_so_far;
    if (wait < time_left) {
      return wait;
    }
    return time_left;
  }

  addMissedClick() {
    this.missed++;
    this.total++;
    this.extra = '\n\n**You missed the click.**';
  }

  addRightClick() {
    this.right++;
    this.missed--;
    this.extra = '\n\n**You clicked the correct button.**';
  }

  addWrongClick() {
    this.wrong++;
    this.missed--;
    this.extra = '\n\n**You clicked an incorrect button.**';
  }

  getRight() {
    return this.right;
  }

  getWrong() {
    return this.wrong;
  }

  getMissed() {
    return this.missed;
  }

  getTotal() {
    return this.total;
  }

  getPercentage() {
    return this.right / this.total * 100;
  }

  getExtraText() {
    return this.extra;
  }
}
