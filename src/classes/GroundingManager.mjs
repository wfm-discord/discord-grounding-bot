// src/classes/GroundingManager.mjs
import { ActionRowBuilder, ButtonBuilder, ButtonStyle } from 'discord.js';
import { setTimeout as wait } from 'node:timers/promises';
import Grounding from '../classes/Grounding.mjs';
import _ from 'lodash';

const fast = 1500;
const normal = 1800;
const slow = 2500;
const very_slow = 5000;
const default_message_content = `You're grounded. **Stay right here** and watch this message. Dismissing this message or missing prompts will mean failure. You have *limited time* to answer when prompted, so watch out.`;
const grounding = {};

export default class GroundingManager {

  constructor() {}

  async groundUser(interaction, grounding_duration, speed, for_user, is_random) {
    // Check to see if there is an existing grounding or duration problem
    // If there is, end here.
    if (await this.checkExistingGrounding(interaction, grounding, is_random) || await this.checkDurationForProblems(interaction, grounding_duration)) {
      return;
    }

    // Declare user and userId using interaction.user.id
    let user = interaction.user;
    let userId = interaction.user.id;

    // New Grounding object to track all the stats for this particular grounding
    grounding[userId] = new Grounding(user, grounding_duration, is_random);
    console.log(`${interaction.user.tag} in #${interaction.channel.name} started a ${grounding[userId].getMinutes()} minute grounding.`);

    //wrap whole thing in a try/catch
    try {
      await this.initializeGrounding(interaction);

      let ms_to_respond = this.getMsToRespond(speed);
      let this_duration = await this.processGrounding(interaction, userId, ms_to_respond);

      await this.finishGrounding(interaction, userId, this_duration);

      let reply_message = this.buildFinalMessage(userId, for_user, is_random, ms_to_respond);
      await interaction.followUp({content: reply_message});
      console.log(`${interaction.user.tag} in #${interaction.channel.name} finished their ${grounding[userId].getMinutes()} minute grounding.`);
    } catch(error) {
      console.log(`${interaction.user.tag} in #${interaction.channel.name} errored out of their ${grounding[userId].getMinutes()} minute grounding.`);
      console.error(error);
      interaction.channel.send("Grounding unexpectedly ended.");
    }
    delete grounding[userId];
  }

  async initializeGrounding(interaction) {
    await interaction.deferReply({ephemeral: true });
    await wait(10);
    await interaction.editReply({content: default_message_content});
  }

  async processGrounding(interaction, userId, ms_to_respond) {
    let timer = grounding[userId].getRandomWait(0) + 5000;
    await wait(timer);
    let this_duration = timer + 10;

    while (this_duration < grounding[userId].getGroundingDuration()) {
      grounding[userId].addMissedClick();
      await this.pressTheRightButtonInteraction(interaction, ms_to_respond);
      await interaction.editReply({ content: default_message_content + grounding[userId].getExtraText(), components: []});
      timer = grounding[userId].getRandomWait(this_duration);
      await wait(timer);
      this_duration = this_duration + timer + ms_to_respond;
    }
    return this_duration;
  }

  async finishGrounding(interaction, userId, this_duration) {
    let timer = grounding[userId].getRandomWait(this_duration);
    await wait(timer);
    await interaction.editReply({content: 'We\'re done here. Go on about your business.'});
  }

  buildFinalMessage(userId, for_user, is_random, ms_to_respond) {
    let reply_message = `<@${userId}> has completed a ${grounding[userId].getMinutes()} minute grounding`;

    if (for_user) {
      reply_message += ` for <@${for_user.id}>.`;
    } else {
      reply_message += '.';
    }

    if (is_random) {
      reply_message += '\n**Surprise! :partying_face:** This grounding was started using the "surprise me" command.';
    }

    reply_message += this.getSpeedMessage(ms_to_respond);
    reply_message += `
      Correct clicks: ${grounding[userId].getRight()}
      Incorrect clicks: ${grounding[userId].getWrong()}
      Missed clicks: ${grounding[userId].getMissed()}
      Completion: ${grounding[userId].getPercentage()}%`;
    if (grounding[userId].getPercentage() == 0) {
      reply_message += '\n\n**Oh no, you got a 0%!** Maybe you need to try a slower setting? ' +
        'There\'s an optional speed setting for slower hardware or connections. ' +
        'Try "slow" and if that\'s no good try "very slow". \n\nYou can do it!'
    }
    return reply_message;
  }

  getSpeedMessage(ms_to_respond) {
    if (ms_to_respond === normal) {
      return '';
    }

    let speed = '';
    switch (ms_to_respond) {
      case fast:
        speed = 'fast';
        break;
      case slow:
        speed = 'slow';
        break;
      case very_slow:
        speed = 'very slow';
        break;
    }
    return `\nClick speed was ${speed}.`;
  }

  async checkExistingGrounding(interaction, grounding, is_random) {
    // Check to see if user already is in a grounding.
    // I'm only allowing one grounding at a time so
    //  the grounding array can be keyed on user id
    let userId = interaction.user.id;
    if (grounding[userId]) {
      let reply = 'You can only do one grounding at a time. Wait.\n';
      if (is_random) {
        reply += 'You can start another one when your \'surprise me\' session is done.';
      } else {
        reply += 'You can start another one in '
          + grounding[userId].getMinutesLeft()
          + ' minutes.';
      }
      await interaction.reply(reply);
      return true;
    }
    return false;
  }

  async checkDurationForProblems(interaction, grounding_duration) {
    if (grounding_duration > 12) {
      await interaction.reply('The maximum you can do at this time is 12 minutes.');
      return true;
    }
    return false;
  }

  async buttonPress(interaction) {
    let userId = interaction.user.id;
    const button = interaction.customId;

    // Check if the button can be pressed (cooldown period)
    if (!grounding[userId].canPressButton()) {
      console.log('Interaction was spam clicked by ' + userId + '.');
      return; // Ignore this press due to cooldown
    }

    if (button === 'goodButton') {
      grounding[userId].addRightClick();
      try {
        await interaction.update({ content: default_message_content + grounding[userId].getExtraText(), components: []});
      } catch (error) {
        if (error.code === 40060) {
          console.log('Interaction has already been acknowledged.');
          return;
        }
        throw error;
      }
    } else {
      grounding[userId].addWrongClick();
      try {
        await interaction.update({ content: default_message_content + grounding[userId].getExtraText(), components: []});
      } catch (error) {
        await this.handleError(interaction, userId, error);
      }
    }
  }

  async pressTheRightButtonInteraction(interaction, speed) {
    let userId = interaction.user.id;
    let random = _.random(1, 5);
    const buttons = [];
    for (let i = 1; i <= 5; i++) {
      buttons.push(new ButtonBuilder()
        .setCustomId(i === random ? 'goodButton' : `button${i}`)
        .setLabel(i.toString())
        .setStyle(ButtonStyle.Primary));
    }
    const row = new ActionRowBuilder().addComponents(buttons);
    try {
      await interaction.editReply({ content: `Press the correct button: **${random}**.`, components: [row] });
    } catch (error) {
      await this.handleError(interaction, userId, error);
      if (error.code === 40060 || error.code === 10062) {
        return;
      }
    }
    await wait(this.getMsToRespond(speed));

    await interaction.editReply({ content: default_message_content + grounding[userId].getExtraText(), components: []});
  }

  async handleError(interaction, userId, error) {
    if (error.code === 40060) {
      console.log('Interaction has already been acknowledged.');
      return;
    } else if (error.code === 10062) {
      console.log('Discord lost the interaction.');
      console.log(`${interaction.user.tag} in #${interaction.channel.name} errored out of their ${grounding[userId].getMinutes()} minute grounding.`);
      console.error(error);
      interaction.channel.send("Grounding unexpectedly ended; Discord lost track of this interaction.");
      return;
    }
    console.log(`${interaction.user.tag} in #${interaction.channel.name} errored out of their ${grounding[userId].getMinutes()} minute grounding.`);
    console.error(error);
    interaction.channel.send("Grounding unexpectedly ended.");
  }

  getMsToRespond(speed) {
    let ms_to_respond = +speed;
    if (!ms_to_respond) {
      ms_to_respond = normal;
    }
    return ms_to_respond;
  }

  getGrounding() {
    return grounding;
  }
}
