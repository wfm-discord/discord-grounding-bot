// src/bot.mjs
import {bot} from './services/bot.mjs';
import 'dotenv/config';
import path from 'node:path';
import { Client, GatewayIntentBits } from 'discord.js';
import GroundingManager from './classes/GroundingManager.mjs';
import {registerCommands} from 'bot-commons-utils/src/utils/botSetup/registerCommands.mjs';
import {registerEvents} from 'bot-commons-utils/src/utils/botSetup/registerEvents.mjs';
import {loginBot} from 'bot-commons-utils/src/utils/botSetup/loginBot.mjs';
import {setupGracefulShutdown} from 'bot-commons-utils/src/utils/botSetup/setupGracefulShutdown.mjs';

// Create a new bot.client instance
bot.client = new Client({ intents: [GatewayIntentBits.Guilds] });

// Define the bot's root directory
bot.botRoot = path.resolve('.');

// Register all commands in the folder
await registerCommands(bot);

// Set up GroundingManager
const groundingManager = new GroundingManager();
bot.groundingManager = groundingManager;

// Register events
await registerEvents(bot);

// Log bot into Discord, set Guild in Bot service
await loginBot(bot);

// Mostly needed for Reaction Role manager (which this bot doesn't have), but still nice to have
// Stop the bot when the process is closed (via Ctrl-C).
setupGracefulShutdown(bot);
